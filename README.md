
Tanglewood is a puzzle/adventure computer game produced by Microdeal in 1987. It was initially released for the Dragon 32 and TRS-80 Color Computer in early 1987 and later converted for the 16-Bit Atari ST and Amiga in 1988.

## Development
The game had originally been planned to be based on the children's TV series Willo the Wisp but this was dropped.[1] It was designed and programmed by Ian Murray-Watson with graphics by Pete Lyon.

## Open Source

The Game had been available at various Abandonware sites for many years, but in 2018 it was kindly released under the MIT License, after asking permission from Ian Murray-Watson, Pete Lyon and former Microdeal publisher John Symes. This is an ongoing effort to release the Microdeal/Michtron-titles for legal preservation.

Sadly, the source code for the game might have been lost in time, so all we can offer at the moment is the image files for usage in an emulator. 


## Help!!

- Are you a former Microdeal/Michtron contact or worker and happen have an old backup of the source code for any of the platforms? Please  help us and send it to our email. It is totally fine to send it anonymously.
- Do you want to resource the game to restore the source code? Go ahead, we encourage you, and as the game is released under MIT it is perfectly fine.

## Many thanks to 

John Symes, Ian Murray-Watson, Pete Lyon for creating this wonderful game, and being permissive and supporting of preserving their work under liberal usage and license.

Thanks to Hall of light, AtariMania and ColorcomputerArchive.

## References

[Wikipedia](https://en.wikipedia.org/wiki/Tanglewood_(video_game))

[AtariManiai (Atar)](http://www.atarimania.com/game-atari-st-tanglewood_10471.html)

[Colorcomputearchive (Dragon32/Tandy)](http://www.colorcomputerarchive.com/coco/Documents/Manuals/Games/)

[Hall of light (Amiga)](http://hol.abime.net/1311)

